from django.urls import path
from django.views.generic import RedirectView

from .views import parse, clear_table


urlpatterns = [
    path('parser/', parse, name='parse'),
    path('clear_table/', clear_table, name='clear_table'),
    path('', RedirectView.as_view(pattern_name='parse'))
]

