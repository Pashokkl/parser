# Generated by Django 3.1.4 on 2023-10-24 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company', models.CharField(max_length=250)),
                ('fact_Qliq_data1', models.IntegerField()),
                ('fact_Qliq_data2', models.IntegerField()),
                ('fact_Qoil_data1', models.IntegerField()),
                ('fact_Qoil_data2', models.IntegerField()),
                ('forecast_Qliq_data1', models.IntegerField()),
                ('forecast_Qliq_data2', models.IntegerField()),
                ('forecast_Qoil_data1', models.IntegerField()),
                ('forecast_Qoil_data2', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Компания',
                'verbose_name_plural': 'Компании',
                'db_table': 'company',
            },
        ),
    ]
