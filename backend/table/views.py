from django.shortcuts import render
from django.http import HttpResponseRedirect
import pandas as pd
from .utils import parser, write_db, add_date_df
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from .models import Company


def parse(request):
    context: dict = {}
    if request.method == 'POST':
        load_file = request.FILES['uploaded_file']  # Выбранный файл
        try:
            df: pd.DataFrame = parser(load_file)  # Парсим данные
            write_db(df)  # Записываем df в БД
        except (ValueError, ValidationError) as err:
            return render(request, 'ursip_parser/start_page.html', {'error_message': err})

        pivoted_df = add_date_df(df)  # Добавляем случайные даты к исходному DataFrame

        df_html = df.to_html()  # Преобразуем DataFrame с датами как таблицу HTML
        pivoted_html = pivoted_df.to_html()  # Преобразуем расчетный DataFrame в виде сводной табл. в HTML

        context = {'df_html': df_html,
                   'pivoted_html': pivoted_html}
    return render(request, 'parser/start_page.html', context)


def clear_table(request):
    Company.objects.all().delete()
    return HttpResponseRedirect(reverse_lazy('parse'))




