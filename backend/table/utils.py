import sqlite3
from random import randrange

import numpy as np
import pandas as pd
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import InMemoryUploadedFile


def write_db(df: pd.DataFrame) -> None:
    """
    Функция производит запись в таблицу 'company' переданного DataFrame
    """
    conn = sqlite3.connect(settings.BASE_DIR / 'db.sqlite3')
    df.to_sql('company', conn, if_exists='append', index=False)
    conn.commit()
    conn.close()


def parser(uploaded_file: InMemoryUploadedFile) -> pd.DataFrame:
    """
    Функция производит парсинг загруженного файла.
    Результатом выполнения является сформированный DataFrame c заговком в одну строку
    """

    # Загружаем файл и заполняем nan ячейки
    df = pd.read_excel(uploaded_file, header=None).fillna(method='ffill', axis=1)
    # Формируем новый заголовок из 3 первых строк
    total_heading = df.head(3)
    new_header = []
    for col, data_col in total_heading.items():
        new_col_name = '_'.join(str(cell) for cell in data_col if str(cell) != 'nan')
        new_header.append(new_col_name)
    # Заменяем на новый заголовок в DataFrame
    df.columns = new_header
    # Проверяем на соотетствие ожидаемого заговка
    if new_header != ['id',
                      'company',
                      'fact_Qliq_data1', 'fact_Qliq_data2',
                      'fact_Qoil_data1', 'fact_Qoil_data2',
                      'forecast_Qliq_data1', 'forecast_Qliq_data2',
                      'forecast_Qoil_data1', 'forecast_Qoil_data2']:
        raise ValidationError(message='Ошибка: Неправильный заголовок')

    # Удаляем старый заголовок состоящий из первых трёх строк и столбец с 'id'
    df = df.drop([0, 1, 2], axis=0).drop('id', axis=1)
    # Сброс индекса
    df = df.reset_index(drop=True)
    return df


def add_date_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Функция егнерирует случайные даты в диапазоне 10.03.2023-20.03.2023 в новый столбец 'date'.
    Формируем сводную таблицу по датам с суммарными итоговыми значениями.
    Добавляем новые столбцы 'fact_Qliq', 'fact_Qoil', 'forecast_Qliq', 'forecast_Qoil' в DataFrame.
    """
    # Генерируем случайные даты
    df['date'] = df.apply(lambda x: str(randrange(10, 20)) + '.03.2023', axis=1)
    # Добавляем новые столбцы
    df_new_col = df.assign(fact_Qliq=df['fact_Qliq_data1'] + df['fact_Qliq_data2'],
                       fact_Qoil=df['fact_Qoil_data1'] + df['fact_Qoil_data2'],
                       forecast_Qliq=df['forecast_Qliq_data1'] + df['forecast_Qliq_data2'],
                       forecast_Qoil=df['forecast_Qoil_data1'] + df['forecast_Qoil_data2']
                       )
    df_pivoted = pd.pivot_table(df_new_col,
                                index='date',
                                values=['fact_Qliq_data1', 'fact_Qliq_data2',
                                        'fact_Qoil_data1', 'fact_Qoil_data2',
                                        'forecast_Qliq_data1', 'forecast_Qliq_data2',
                                        'forecast_Qoil_data1', 'forecast_Qoil_data2',
                                        'fact_Qliq', 'fact_Qoil',
                                        'forecast_Qliq', 'forecast_Qoil'
                                        ],
                                aggfunc=np.sum,
                                margins=True,
                                margins_name='Total',
                                )
    return df_pivoted

